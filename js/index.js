$(function()){
  $("[data-toggle='tooltip']").tooltip();
  $("[data-toggle='popover']").popover();
  $('#contacto').on('show.bs.modal', function(e){
    console.log('El modal contacto se està mostrando');
    $('#contactoBtn').removeClass('btn-outline success');
    $('#contactoBtn').addClass('btn-primary');
    $('#contactoBtn').prop('disabled',true);
  });
  $('#contacto').on('shown.bs.modal', function(e){
    console.log('El modal contacto se mostrò');
  });
  $('#contacto').on('hidden.bs.modal', function(e){
    console.log('El modal contacto se ocultò');
    $('#contactoBtn').prop('disabled', false);
  })
});
