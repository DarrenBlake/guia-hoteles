module.exports = function (grunt){
  grunt.initconfig({
    sass:{
      dist:{
        files:[{
          expand:true,
          cwd: 'css',
          src: ['*.scss'],
          dest:'css',
          ext: '.css'
        }]
      }
    },
    watch:{
      files:['css/*.scss'],
      tasks:['css']
    },
    browsersync:{
      dev:{
        bsfiles:{//browser files
        src:[
          'css/*.css',
          '*.html',
          'js/*.js'
        ]
      },
      options:{
        watchtask: true,
        server:{
          basedir:'./' //directorio base para nuestro servidor
        }
      }
      }
    }
  });
  grunt.loadNpmtasks('grunt-contrib-watch');
  grunt.loadNpmtasks('grunt-contrib-sass');
  grunt.registertask('css',['sass']);
};
